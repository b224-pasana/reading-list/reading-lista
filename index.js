console.log("Hello World");

/*
	1. Write a JS function that returns a passed string with letters in alphabetical order.

		Example String: 'webmaster'
		Expected output: 'abeemrstw'
*/

//Solution:

		function sortAtoZ(string){
		//split() - convert string to array.
			//sort() - sort the array.
				//join() - convert the array to string
				return string.split('').sort().join('');
		};

		console.log(sortAtoZ('webmaster'));


/*
	2. Write a JS function that accepts a string as a parameter and counts the number of vowels within string.

		Example String: 'The quick brown fox'
		Expected output: '5'
*/

//Solution:


		function countVowels(string){
			//add all the vowels in a variable
			let vowels = ['a', 'e', 'i', 'o', 'u'];
			//preparing a variable container for the vowels counts
			let count = 0;

				for (let n = 0; n < string.length; n++) {
								//if the string.lenth vowel.includes() = true, then count++
								if(vowels.includes(string[n].toLowerCase())){
									count++;
								}
							};
							//return the count value
							return count;
						};


						let vowelCount = countVowels('The quick brown fox');

						console.log(vowelCount);

/*
	3. Write a JS function that inserts new elements at the beginning of the array and sorts them in alphabetical order. Add the rest of the member countries of South East Asia.

		Sample Array:
			let countries = ["Philippines", "Indonesia", "Vietnam", "Thailand"]

*/

//Solution:

				let countries = ["Philippines", "Indonesia", "Vietnam", "Thailand"];

				function addCountry(newCountry) {
						//add the argument at the beginning of the array
						countries.unshift(newCountry);
						//use array.sort() method to sort the argument alphabetically and return the result
						return countries.sort();
				};

				addCountry("Cambodia");
				addCountry("Singapore");
				addCountry("Brunei");
				addCountry("Myanmar");
				addCountry("Timor-Leste");
				addCountry("Laos");
				addCountry("Malaysia");

				console.log(countries);

/*
	4. Using object literals, create a person object with the following properties: firstName, lastName, age, gender, nationality.

		Print the object in the console.
*/

//Solution:
				let person = {
					firstName: "Erlyn Joy",
					lastName: "Solasco",
					age: 26,
					gender: "Female",
					nationality: "Filipino"
				};

				console.log(person);


/*
	5. Using constructor function, create 3 objects with the same structure. These 3 objects must have atleast 5 key-value pairs, and 1 of the property must be a method that allows the object to print a message in the console.
*/


//Solution:
				function mobilePhone (brand, operatingSystem, processor, storage, specsUrl) {
					this.brand = brand;
					this.operatingSystem = operatingSystem;
					this.processor = processor;
					this.storage = storage;
					this.specsUrl = specsUrl;
					this.myPhone = function() {
					    return `${this.brand} 
			Operating System : ${operatingSystem}
			Processor : ${processor}
			Storage : ${storage}
			For more details please visit:
			${specsUrl}`
					  };
				};

				let mobile1 = new mobilePhone("Apple Iphone 14 Pro Max", "iOS 16", "Hexa-core", "128GB 6GB RAM", 'https://www.gsmarena.com/apple_iphone_14_pro_max-11773.php',);
				console.log(mobile1.myPhone());

				let mobile2 = new mobilePhone("Google Pixel 7 Pro", "Android 13", "Octa-core", "128GB 8GB RAM", 'https://www.gsmarena.com/google_pixel_7_pro-11908.php',);
				console.log(mobile2.myPhone());

				let mobile3 = new mobilePhone("Apple iPhone 13", "iOS 15", "Hexa-core", "128GB 4GB RAM", 'https://www.gsmarena.com/apple_iphone_13-11103.php',);
				console.log(mobile3.myPhone());
